﻿// Lesson_17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "Animal voice" << endl;
	}
};

class Dog :public Animal
{
public:

	  void Voice() override
	{
	 	
		cout << "Gaff....Gaff...Gaff" << endl;
	}
};

class Cat :public Animal
{
public:

	void Voice() override
	{

		cout << "Myaoo..Myaoo...Myaoo" << endl;
	}
};

class Cow :public Animal
{
public:

	void Voice() override
	{

		cout << "Moo..Moo...Moo" << endl;
	}
};



int main()
{
	setlocale(LC_ALL, "ru");
	
	Animal* Array[]
	{
		new Cat,
		new Dog,
		new Cow
	};
	
	for (int i = 0; i <sizeof(Array)/sizeof(Array[0]); i++)
	{
		Array[i]->Voice();
	}
	return 0;

}